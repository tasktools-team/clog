clog (1.3.0+dfsg-1) unstable; urgency=medium

  * Team upload.

  [ Debian Janitor ]
  * Trim trailing whitespace.
  * Use secure copyright file specification URI.
  * Update watch file format version to 4.
  * Bump debhelper from deprecated 9 to 13.
  * Set debhelper-compat version in Build-Depends.
  * Change priority extra to priority optional.
  * Update Vcs-* headers from URL redirect.
  * Use canonical URL in Vcs-Git.

  [ Andreas Tille ]
  * Replace broken Homepage by Github
    Closes: #1060737
  * Point watch file to Github
  * Exclude binaries from source code via Files-Excluded
  * Remove Iain R. Learmonth from Uploades (Thank you Ian for offering
    help if needed)
    Closes: #986957
  * Standards-Version: 4.7.0 (routine-update)

 -- Andreas Tille <tille@debian.org>  Thu, 09 Jan 2025 10:11:14 +0100

clog (1.3.0-1.1) unstable; urgency=medium

  * Non-maintainer upload.
  * Fix FTBFS with gcc13 by adding a missing include. (Closes: #1037606)
    - Thanks Mate Kukri and Jonathan Bergh. (LP: #2048084)

 -- Sudip Mukherjee <sudipm.mukherjee@gmail.com>  Fri, 05 Jan 2024 11:40:11 +0000

clog (1.3.0-1) unstable; urgency=medium

  * New upstream release.
  * debian/control:
   - Now maintained by the Debian Tasktools Packaging Team.
   - Uploaders set to alejandro and myself.
   - Updated Vcs-* fields for pkg-tasktools git repository.
   - Standards-Version to 3.9.8.
  * debian/rules:
   - Enable build hardening.
  * debian/patches/FixManMinusSign.diff:
   - Refreshed patch, some hyphens had moved.

 -- Iain R. Learmonth <irl@debian.org>  Thu, 07 Jul 2016 23:14:03 +0100

clog (1.1.0-3) unstable; urgency=medium

  * Fix wrong upstream Homepage URL. Thanks Matanya Moses (Closes: #739105).
  * Update Standards-Version field in d/control.

 -- Alejandro Garrido Mota <alejandro@debian.org>  Sun, 20 Apr 2014 12:50:22 -0430

clog (1.1.0-2) unstable; urgency=low

  * d/control: Improve package description (short and long). Thanks
    to Justin B Rye <justin.byam.rye@gmail.com> (Closes: #724846).

 -- Alejandro Garrido Mota <alejandro@debian.org>  Mon, 30 Sep 2013 06:34:49 -0430

clog (1.1.0-1) unstable; urgency=low

  * Initial release (Closes: #722178).

 -- Alejandro Garrido Mota <alejandro@debian.org>  Mon, 02 Sep 2013 22:42:34 +0200
